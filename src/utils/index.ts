export const treatCPF = (value: string) => (
    value
        .substring(0,14)
        .replace(/\D/g, '')
        .replace(/(\d{3})(\d{1,})/g, '$1.$2')
        .replace(/(\d{3}\.\d{3})(\d{1,})/g, '$1.$2')
        .replace(/(\d{3}\.\d{3}\.\d{3})(\d)/g, '$1-$2')
        
)

export const treatDate = (value: string) => (
    value
        .substring(0,10)
        .replace(/\D/g, '')
        .replace(/(\d{2})(\d{1,})/g, '$1/$2')
        .replace(/(\d{2}\/\d{2})(\d{1,})/g, '$1/$2')
)

export const isCPF = ( testText: string ) => {
    let cleanText = testText.replace(/\D/g,'');
    if( cleanText.length === 11 ){
        const num0 = parseInt( cleanText[0])
        const num1 = parseInt( cleanText[1])
        const num2 = parseInt( cleanText[2])
        const num3 = parseInt( cleanText[3])
        const num4 = parseInt( cleanText[4])
        const num5 = parseInt( cleanText[5])
        const num6 = parseInt( cleanText[6])
        const num7 = parseInt( cleanText[7])
        const num8 = parseInt( cleanText[8])
        const num9 = parseInt( cleanText[9])
        const num10 = parseInt( cleanText[10])
        if( num0 === num1 
            && num1 === num2 
            && num2 === num3 
            && num3 === num4 
            && num4 === num5 
            && num5 === num6 
            && num6 === num7 
            && num7 === num8 
            && num8 === num9 
            && num9 === num10 
        ){
            return false;
        } else {
            const sum1 = num0 * 10 + num1 * 9 + num2 * 8 + num3 * 7 + num4 * 6 + num5 * 5 + num6 * 4 + num7 * 3 + num8 * 2;
            let mod1 = (sum1 * 10) % 11;
            if( mod1 === 10 ){ mod1 = 0 }
    
            const sum2 = num0 * 11 + num1 * 10 + num2 * 9 + num3 * 8 + num4 * 7 + num5 * 6 + num6 * 5 + num7 * 4 + num8 * 3 + num9 * 2;
            let mod2 = (sum2 * 10) % 11;
            if( mod2 === 10 ){ mod2 = 0 }
            if( mod1 === num9 && mod2 === num10 ){
                return true;
            }else{
                return false;
            }
        }
    }else{
        return false;
    }
}