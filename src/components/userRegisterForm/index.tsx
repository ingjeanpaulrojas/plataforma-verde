import React from 'react';
import PageTop from '../pageTop';
import Form from '../form';
import List from '../list';
import { Container } from './styles';
import { FormContainer } from '../form/styles';

const UserRegisterForm: React.FC = () => (
    <Container>
        <PageTop />
        <FormContainer>
            <Form />
            <List />
        </FormContainer>
    </Container>
)

export default UserRegisterForm;