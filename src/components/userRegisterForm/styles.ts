import styled from 'styled-components';

export const Container = styled.div`
    padding-inline: 50px;
    padding-block: 35px;
    background-color: #FFFFFF;
`