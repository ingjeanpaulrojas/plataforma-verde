import React from 'react';
import { info, user } from '../../assets/icons';
import InfoPopUp from '../infoPopUp';
import { CenterRow, PageHeader, Text, Title, Icon, Container } from './styles';

const PageTop: React.FC = () => (
    <PageHeader>
        <CenterRow>
            <Icon size={60} src={user} alt="user icon" />
            <Title size={25}>Cadastro Usuário</Title>
            <Container>
                <Icon size={16} src={info} alt="info icon" />
                <InfoPopUp />
            </Container>
        </CenterRow>
        <Text size={13}>* campos obrigatórios</Text>
    </PageHeader>
)

export default PageTop;