import styled from 'styled-components';

export const FlexRow = styled.div`
    display: flex;
    flex-direction: row;
`

export const CenterRow = styled(FlexRow)`
    align-items: center;
`

export const PageHeader = styled(FlexRow)`
    justify-content: space-between;
    align-items: flex-end;
    padding-bottom: 10px;
    border-bottom: 1px solid #CCCCCC;
    margin-bottom: 24px;
`

export const Text = styled('span')<{ size: number }>`
    font-size: ${props => props.size}px;
`

export const Title = styled(Text)<{ size: number }>`
    font-size: ${props => props.size}px;
    padding-inline: 10px;
`

export const Icon = styled('img')<{ size: number }>`
    height: ${props => props.size}px;
    width: ${props => props.size}px;
`

export const Container = styled.div`
    position: relative;

    &:hover .infoPopUp{
        visibility: visible;
    }
`