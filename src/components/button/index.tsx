import React from 'react';
import { Icon } from '../pageTop/styles';
import { Button, ButtonLabel } from './styles';

interface SubmitButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    label: string,
    icon: string
}

const SubmitButton = (props: SubmitButtonProps) => (
    <Button {...props}>
        <Icon size={14} src={props.icon} alt="user icon" />
        <ButtonLabel>{props.label}</ButtonLabel>
    </Button>
)

export default SubmitButton;