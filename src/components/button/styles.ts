import styled from 'styled-components';

export const Button = styled.button`
    &:active {
        background-color: #407717;
    }
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 30px;
    background-color: #5CAC21;
    border-radius: 4px;
    border: none;
    margin-bottom: 10px;
`
export const ButtonLabel = styled.span`
    margin-inline: 12px;
    font-size: 12px;
    color: white;
    font-weight: bold;
    text-transform: uppercase;
`
