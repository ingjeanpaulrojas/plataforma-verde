import styled from 'styled-components';

export const Container = styled.div`
    visibility: hidden;
    width: 221px; 
    height: 51px; 
    background-color: #F0F0F0; 
    box-shadow: 1px 1px 2px #00000043; 
    border-radius: 5px; 
    padding: 5px; 
    // isto vai ajudar a que não fique oculto o icono de info
    position: absolute;
    top: -5px;
    left: 20px;
    display: flex;
    flex-direction: row;
`

export const Text = styled.span`
    font-family: Open Sans;
    font-size: 12px;
    color: #505050;
    margin-left: 10px;
`
