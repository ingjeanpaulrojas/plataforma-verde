import React from 'react';
import { Container, Text } from './styles';
import { Icon } from '../pageTop/styles';
import { redx } from '../../assets/icons';

const InfoPopUp: React.FC = () => (
    <Container className='infoPopUp'>
        <Icon size={16} src={redx} alt={'red X'} />
        <Text>Cadastre os usuários que terão acesso ao sistema.</Text>
    </Container>
)

export default InfoPopUp;