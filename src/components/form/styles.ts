import styled from 'styled-components';

export const FormContainer = styled.div`
    padding-top: 15px;
    padding-inline: 20px;
    padding-bottom: 27px;
    background-color: #F5F5F5;
    box-shadow: 0px 2px 2px #00000067;
    border-radius: 4px;
`