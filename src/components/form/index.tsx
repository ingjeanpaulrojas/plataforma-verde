import { DateTime } from 'luxon';
import React, { useEffect } from 'react';
import { Col, Grid, Row } from 'react-flexbox-grid';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import { arrowDown } from '../../assets/icons';
import { addRow, editRow, setFormData, setFormInfo } from '../../redux/actions/appActions';
import { RootState } from '../../redux/reducers';
import { IListRow } from '../../redux/reducers/types';
import { getCities, getStates } from '../../redux/thunks/appThunk';
import { isCPF, treatCPF, treatDate } from '../../utils';
import SubmitButton from '../button';
import { SelectInput, TextInput } from '../input';

const UserRegisterForm: React.FC = () => {
    const { form, cities, states, editMode } = useSelector( (state: RootState) => state.app )
    const dispatch = useDispatch(); 
    useEffect( () => {
        dispatch( getStates() )
    }, [dispatch] )

    useEffect( () => {
        if( form.selectedState !== '' ){
            dispatch( getCities(form.selectedState) )
        }
    }, [form.selectedState, dispatch] )
    const handleFormChange = ( target: string, value: string ) => {
        dispatch( setFormData(
            target, 
            target === 'cpf' ? treatCPF(value) :
            target === 'birth_date' ? treatDate(value) : 
            value
        ))
    }
    const handleSaveRow = () => {
        const values = Object.values(form);
        if( values.findIndex( item => item === '' ) > -1 ){
            alert('Não pode deixar campos em branco.');
            return;
        }
        if( form.cpf.length < 14 || !isCPF(form.cpf) ){
            alert('O campo CPF está mal formado ou não é um CPF válido, por favor corrija.');
            return;
        }
        if( isNaN(DateTime.fromFormat(form.birth_date, 'dd/LL/yyyy').day) ){
            alert('A data inserida não é valida.');
            return;
        }
        const foundCity = cities.find( item => item.nome === form.selectedCity );
        const foundState = states.find( item => item.sigla === form.selectedState );
        if( foundCity && foundState ){
            const data: IListRow = { 
                ...form, 
                id: editMode.isEditing ? editMode.editId : uuidv4(),
                cidade: {
                    id: foundCity.id,
                    nome: foundCity.nome
                },
                estado: {
                    id: foundState.id,
                    nome: foundState.nome,
                    sigla: foundState.sigla
                }
            }
            if( editMode.isEditing ){
                dispatch( editRow( editMode.editId, data) )
            }else{
                dispatch( addRow(data) )
            }
            dispatch( setFormInfo({ name: '', cpf: '', birth_date: '', selectedCity: '', selectedState: '' }) );
        }
    }
    return (
        <>
        <Grid fluid style={{ paddingInline: 0 }}>
            <Row style={{ marginBottom: 25 }}>
                <Col xl={7} lg={6} md={6} sm={12}>
                    <TextInput label={'Nome Completo*'} value={form.name} onChange={(e) => handleFormChange('name', e.target.value)} />
                </Col>
                <Col xl={3} lg={3} md={3} sm={6}>
                    <TextInput label={'CPF*'} placeholder={'000.000.000-00'} value={form.cpf} onChange={(e) => handleFormChange('cpf', e.target.value)} />
                </Col>
                <Col xl={2} lg={3} md={3} sm={6}>
                    <TextInput label={'Data de Nascimento*'} placeholder={'dd/mm/aaaa'} value={form.birth_date} onChange={(e) => handleFormChange('birth_date', e.target.value)} />
                </Col>
            </Row>
            <Row bottom="xs">
                <Col lg={5} md={4} sm={6}>
                    <SelectInput label={'Estado*'} options={states} value={form.selectedState} onChange={(e) => {
                        handleFormChange('selectedState', e.target.value)
                        handleFormChange('selectedCity', '')
                    }} />
                </Col>
                <Col lg={5} md={4} sm={6}>
                    <SelectInput label={'Cidade*'} options={cities} value={form.selectedCity} onChange={(e) => handleFormChange('selectedCity', e.target.value)} />
                </Col>
                <Col lg={2} md={4} sm={12}>
                    <SubmitButton onClick={handleSaveRow} label={editMode.isEditing ? 'Editar' : 'Incluir'} icon={arrowDown} />
                </Col>
            </Row>
        </Grid>
    </>
)}

export default UserRegisterForm;