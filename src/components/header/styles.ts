import styled from 'styled-components';

export const Container = styled.div`
    height: 80px;
    background-color: #EFEFEF;
`

export const Logo = styled.img`
    padding: 20px;
    width: 145.73px;
    height: 37.73px;
`