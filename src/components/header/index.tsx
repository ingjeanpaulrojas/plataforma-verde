import React from 'react';
import { logo } from '../../assets/icons';
import { Container, Logo } from './styles';

const Header: React.FC = () => (
    <Container>
        <Logo src={logo} alt='logo image'/>
    </Container>
)

export default Header;