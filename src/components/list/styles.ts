import styled from 'styled-components';

export const Table = styled.table`
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: transparent;
    border-collapse: collapse;

    // FOR ALL
    & td {
        border: 1px solid #D0D0D0;
        padding-block: 11px;
        padding-inline: 10px;
    }

    // FOR HEAD
    & thead tr{
        background-color: #E0E0E0;
    }

    & thead tr td{
        color: #101010;
        font-weight: 900;
        font-size: 13px;
    }

    & thead tr td:nth-child(1){     min-width: 250px;     }
    & thead tr td:nth-child(2){     min-width: 140px;     }
    & thead tr td:nth-child(3){     min-width: 180px;     }
    & thead tr td:nth-child(5){     min-width: 108px;     }
    & thead tr td:nth-child(6){     min-width: 257px;     }
    
    & thead tr td:nth-child(4),
    & thead tr td:nth-child(7),
    & thead tr td:nth-child(8){     min-width: 100px;     }

    // FOR BODY
    & tbody tr td{
        font-size: 12px;
        color: #303030;
        background-color: #FFFFFF;
    }

    & tbody tr:hover td{
        background-color: #F5F5F5;
    }

    & tbody tr td:nth-child(7),
    & tbody tr td:nth-child(8){
        text-align: center
    }
`

export const Container = styled.div`
    margin-top: 20px;
    display: block;
    overflow-x: auto;
    width: 100%;
`