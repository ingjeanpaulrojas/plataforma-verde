import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/reducers';
import { Table, Container } from './styles';
import { DateTime } from 'luxon';
import { Icon } from '../pageTop/styles';
import { edit, trash } from '../../assets/icons';
import { IListRow } from '../../redux/reducers/types';
import { deleteRow, setFormInfo, toggleEditing } from '../../redux/actions/appActions';

const List: React.FC = () => {
    const { list } = useSelector( (state: RootState) => state.app )
    const dispatch = useDispatch();
    const handleErase = ( item: IListRow ) => {
        if( window.confirm(`Tem certeza que deseja exluir o registro de ${item.name}?`) ){
            dispatch( deleteRow(item.id) )
            dispatch( setFormInfo({ name: '', cpf: '', birth_date: '', selectedCity: '', selectedState: '' }) );
            dispatch( toggleEditing(false, '') );
        }
    }
    const handleEdit = ( item: IListRow ) => {
        dispatch( toggleEditing( true, item.id ) )
        const { birth_date, cidade, cpf, estado: { sigla }, name } = item;
        dispatch( setFormInfo({ name, birth_date, cpf, selectedCity: cidade.nome, selectedState: sigla }) )
    }
    return (
        <Container>
            <Table>
                <thead>
                    <tr>
                        <td>NOME COMPLETO</td>
                        <td>CPF</td>
                        <td>DATA DE NASCIMENTO</td>
                        <td>IDADE</td>
                        <td>ESTADO</td>
                        <td>CIDADE</td>
                        <td>EDITAR</td>
                        <td>EXCLUIR</td>
                    </tr>
                </thead>
                <tbody>
                    { list.length > 0 ? list.map( item => (
                        <tr key={item.id}>
                            <td>{item.name}</td>
                            <td>{item.cpf}</td>
                            <td>{item.birth_date}</td>
                            <td>{Math.round( DateTime.now().diff(DateTime.fromFormat( item.birth_date, 'dd/LL/yyyy'),'years').years)}</td>
                            <td>{item.estado.sigla}</td>
                            <td>{item.cidade.nome}</td>
                            <td>
                                <Icon size={18} src={edit} onClick={() => handleEdit(item)} />
                            </td>
                            <td>
                                <Icon size={18} src={trash} onClick={() => handleErase(item)} />
                            </td>
                        </tr>
                    ) ) : 
                    <tr><td style={{ textAlign: 'center' }} colSpan={8}>NÃO TEM USUARIOS GUARDADOS</td></tr> }
                </tbody>
            </Table>
        </Container>
)}

export default List;