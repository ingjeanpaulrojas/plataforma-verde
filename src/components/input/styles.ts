import styled, { css } from 'styled-components';

export const Label = styled.label`
    color: #202020;
    font-size: 14px;
    display: block;
    margin-bottom: 6px;
`

export const baseInputStyles = css`
    background-color: white;
    margin-bottom: 10px;
    font-size: 15px;
    color: #202020;
    display: block;
    width: 100%;
    height: 30px;
    border: 1px solid #D0D0D0;
    border-radius: 4px;
    padding-block: 5px;
    padding-inline: 10px;
`

export const Input = styled.input`
    ${baseInputStyles}
`

export const Select = styled.select`
    ${baseInputStyles}
`