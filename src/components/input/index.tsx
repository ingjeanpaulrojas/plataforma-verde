import React from 'react';
import DatePicker, { DatePickerProps } from 'react-date-picker';
import { ICity, IState } from '../../redux/reducers/types';
import { Label, Input, Select } from './styles';

interface BaseInputProps {
    label: string,
    target?: string,
}

interface SelectInputProps {
    options: IState[] | ICity[],
}

const TextInput = (props: BaseInputProps & React.InputHTMLAttributes<HTMLInputElement> ) => (
    <>
        <Label>{props.label}</Label>
        <Input {...props} />
    </>
)

const DateInput = (props: BaseInputProps & DatePickerProps) => (
    <>
        <Label>{props.label}</Label>
        <DatePicker calendarIcon={null} clearIcon={null} dayPlaceholder={'dd'} monthPlaceholder={'mm'} yearPlaceholder={'aaaa'} {...props} />
    </>
)

const SelectInput = (props: BaseInputProps & SelectInputProps & React.SelectHTMLAttributes<HTMLSelectElement>) => (
    <>
        <Label>{props.label}</Label>
        <Select {...props}>
            <option value=''>Selecione</option>
            { props.options.length > 0 && props.options.map( item => (
                <option key={item.id} data-value={item.id} value={item.sigla}>{item.nome}</option>
            ) ) }
        </Select>
    </>
)

export { TextInput, DateInput, SelectInput };