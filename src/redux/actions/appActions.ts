import { action } from 'typesafe-actions';
import { ICity, IFormData, IListRow, IState } from '../reducers/types';
import { APP_ACTIONS } from './types';

export const setStates = ( states: IState[] ) => action(APP_ACTIONS.SET_STATES, { states });
export const setCities = ( cities: ICity[] ) => action(APP_ACTIONS.SET_CITIES, { cities });
export const setFormData = ( target: string, value: string ) => action(APP_ACTIONS.SET_FORM_DATA, { target, value });
export const addRow = ( row: IListRow ) => action(APP_ACTIONS.ADD_ROW, { row });
export const setFormInfo = ( form: IFormData ) => action(APP_ACTIONS.SET_FORM_INFO, { form });
export const editRow = ( id: string, row: IListRow ) => action(APP_ACTIONS.EDIT_ROW, { id, row });
export const deleteRow = ( id: string ) => action(APP_ACTIONS.DELETE_ROW, { id });
export const toggleEditing = ( isEditing: boolean, id: string ) => action(APP_ACTIONS.TOGGLE_EDITING, { isEditing, id });

