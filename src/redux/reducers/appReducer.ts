import { Reducer } from 'redux';
import { ActionType } from 'typesafe-actions';
import * as appActions from '../actions/appActions';
import { APP_ACTIONS } from '../actions/types';
import { IAppState } from './types';

const appState: IAppState = {
  editMode: {
    isEditing: false,
    editId: ''
  },
  states: [],
  cities: [],
  form: {
    name: '',
    cpf: '',
    birth_date: '',
    selectedState: '',
    selectedCity: '',
  },
  list: []
};

const appReducer: Reducer<IAppState, ActionType<typeof appActions>> = (state = appState, action) => {
  switch (action.type) {
    case APP_ACTIONS.SET_STATES:            return { ...state, states: action.payload.states };
    case APP_ACTIONS.SET_CITIES:            return { ...state, cities: action.payload.cities };
    case APP_ACTIONS.SET_FORM_DATA:         return { ...state, form: { ...state.form, [action.payload.target]: action.payload.value } };
    case APP_ACTIONS.SET_FORM_INFO:         return { ...state, form: action.payload.form};
    case APP_ACTIONS.ADD_ROW:               return { ...state, list: [ ...state.list, action.payload.row ] };
    case APP_ACTIONS.EDIT_ROW:              
      const copyList = [...state.list];
      const targetIndex = copyList.findIndex( item => item.id === action.payload.id );
      copyList[targetIndex] = action.payload.row
      return { ...state, list: copyList, editMode: { isEditing: false, editId: '' } };
    case APP_ACTIONS.DELETE_ROW:            return { ...state, list: state.list.filter( item => item.id !== action.payload.id )};
    case APP_ACTIONS.TOGGLE_EDITING:        return { ...state, editMode: { editId: action.payload.id, isEditing: action.payload.isEditing }};
    default:
      return { ...state };
  }
};

export default appReducer;
