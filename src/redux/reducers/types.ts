export interface ICity {
    id?: number,
    nome: string,
    sigla?: never,
}

export interface IState {
    id?: number,
    sigla: string,
    nome: string,
}

export interface IListRow extends UserData {
    id: string,
    idade?: number,
    estado: IState,
    cidade: ICity,
}

export interface UserData {
    name: string,
    cpf: string,
    birth_date: string,
}

export interface IFormData extends UserData {
    selectedState: string,
    selectedCity: string,
}

export interface IAppState {
    editMode: {
        isEditing: boolean,
        editId: string
    },
    states: IState[],
    cities: ICity[],
    list: IListRow[],
    form: IFormData,
}
