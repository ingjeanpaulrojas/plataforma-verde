// Vou deixar isto preparado ja que é como uma norma para projetos grandes e é mais facil para mim
import * as redux from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';

import app from './appReducer';

export const rootReducer = redux.combineReducers({
  app,
});

export type AppThunkDispatch = ThunkDispatch<RootState, void, Action>;
export type RootState = ReturnType<typeof rootReducer>;