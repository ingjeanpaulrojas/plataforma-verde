import axios, { AxiosResponse } from 'axios';
import { Dispatch } from 'redux';
import { setCities, setStates } from '../actions/appActions';
import { IState } from '../reducers/types';

const API = axios.create({
    baseURL: 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
})

export const getStates = () => async ( dispatch: Dispatch ) => {
    const resp: AxiosResponse<any> = await API.get('/');
    dispatch( setStates(resp.data.sort( (p: IState, n: IState) => p.nome.localeCompare(n.nome) )) )
}

export const getCities = (stateId: string) => async ( dispatch: Dispatch ) => {
    API.get(`/${stateId}/municipios`).then( (resp) => {
        dispatch( setCities(resp.data) )
    } )
}