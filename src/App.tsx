import React from 'react';
import { Header, UserRegisterForm } from './components';
import GlobalStyle from './globals';
import { Provider } from 'react-redux';
import { store } from './redux/store';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <GlobalStyle />
      <Header />
      <UserRegisterForm />
    </Provider>
  );
}

export default App;
