import arrowDown from './arrow-down.svg';
import logo from './logo.svg';
import trash from './trash.svg';
import edit from './edit.svg';
import user from './user.svg';
import info from './info.svg';
import redx from './red-x.svg';

export { arrowDown, logo, trash, user, info, edit, redx }