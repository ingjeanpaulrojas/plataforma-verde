import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
    font-family: Roboto;
  }

  body {
    -webkit-font-smoothing: antialiased !important;
  }
  
  body html #root {
    height: 100%;
  }

  img {
    box-sizing: content-box;
  }

  // Não tem um lugar mais elegante para colocar os estilos do DatePicker. ^_^

  .react-date-picker {
    color: #202020;
    font-size: 15px;
    width: 100%;
    height: 30px;
    background-color: white;
    border: 1px solid #D0D0D0;
    border-radius: 4px;
    padding-block: 5px;
    padding-inline: 10px;
  }

  .react-date-picker .react-date-picker__wrapper {
    border: transparent;
  }

  .react-date-picker .react-date-picker__inputGroup__divider{
    color: #B0B0B0;
  }
`

export default GlobalStyle;
